1. This project favors Yarn, so make sure you have the latest.

  ```
  $ npm install -g yarn
  ```

2. Install the build and application dependencies.

  ```
  $ yarn install
  ```

3. Launch the server with this command

  ```
  $ yarn start
  ```