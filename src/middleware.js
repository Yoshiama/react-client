import request from './request';
import { LOGIN, LOGOUT, REGISTER } from './reducers/actions';

const promiseMiddleware = () => next => (action) => {
  if (action.payload && typeof action.payload.then === 'function') {
    action.payload.then(
      (res) => {
        action.payload = res;
        action.error = false;
        next(action);
      },
      (error) => {
        action.error = true;
        if (error.response) action.payload = error.response.body;
        next(action);
      },
    );
  } else {
    next(action);
  }
};

const localStorageMiddleware = () => next => (action) => {
  if (action.type === REGISTER || action.type === LOGIN) {
    if (!action.error) {
      window.localStorage.setItem('jwt', action.payload.token);
      request.setToken(action.payload.token);
    }
  } else if (action.type === LOGOUT) {
    window.localStorage.setItem('jwt', '');
    request.setToken(null);
  }
  next(action);
};

export { promiseMiddleware, localStorageMiddleware };
