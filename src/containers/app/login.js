import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { LOGIN, SET_PARAMS, LOGIN_CLEAR } from '../../reducers/actions';
import request from '../../request';

const mapStateToProps = state => ({
  login: state.login,
  general: state.general,
});

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value => dispatch({ type: SET_PARAMS, key: 'email', value }),
  onChangePassword: value =>
    dispatch({ type: SET_PARAMS, key: 'password', value }),
  onSubmit: (email, password) =>
    dispatch({ type: LOGIN, payload: request.Auth.login(email, password) }),
  onUnload: () => dispatch({ type: LOGIN_CLEAR }),
});

// import Request from 'request';

class Login extends React.Component {
  constructor() {
    super();
    this.changeEmail = (ev) => {
      this.props.onChangeEmail(ev.target.value);
    };
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => (ev) => {
      ev.preventDefault();
      this.props.onSubmit(email, password);
    };
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  isLogged() {
    return this.props.general.userInfo;
  }

  render() {
    const { email, password } = this.props.login;

    if (!this.isLogged()) {
      return (
        <form className="sign" onSubmit={this.submitForm(email, password)}>
          <div className="form-group">
            <input
              type="email"
              className="form-control"
              id="email"
              aria-describedby="emailHelp"
              placeholder="Email"
              onChange={this.changeEmail}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              className="form-control"
              id="password"
              placeholder="Password"
              onChange={this.changePassword}
            />
            <small id="emailHelp" className="form-text text-muted">
              Tqt pas frair, file ton mdp, tout va bien se passer.
            </small>
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      );
    }
    return <Redirect to="/" />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
