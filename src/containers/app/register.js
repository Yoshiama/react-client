import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {
  REGISTER,
  SET_PARAMS,
  REGISTER_CLEAR,
  PASSWORD_ERROR,
  AGE_ERROR,
} from '../../reducers/actions';
import request from '../../request';

const mapStateToProps = state => ({
  login: state.login,
  general: state.general,
});

const mapDispatchToProps = dispatch => ({
  onChange: (value, keyy) => dispatch({ type: SET_PARAMS, key: keyy, value }),
  onSubmit: (email, password, name, age) =>
    dispatch({
      type: REGISTER,
      payload: request.Auth.register(email, password, name, age),
    }),
  onUnload: () => dispatch({ type: REGISTER_CLEAR }),
  onPasswordError: () => dispatch({ type: PASSWORD_ERROR }),
  onAgeError: () => dispatch({ type: AGE_ERROR }),
});

class Register extends React.Component {
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChange(ev.target.value, 'email');
    this.changePassword = ev =>
      this.props.onChange(ev.target.value, 'password');
    this.changeName = ev => this.props.onChange(ev.target.value, 'name');
    this.changeAge = ev => this.props.onChange(ev.target.value, 'age');
    this.submitForm = (email, password, name, age) => (ev) => {
      let errNbr = false;
      ev.preventDefault();

      if (password.length <= 9) {
        this.props.onPasswordError();
        errNbr = true;
      }
      if (age <= 18) {
        this.props.onAgeError();
        errNbr = true;
      }
      if (errNbr) return;
      this.props.onSubmit(email, password, name, age);
    };
    this.isLogged = () => !!this.props.general.userInfo;
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { email, password, name, age } = this.props.login;
    if (!this.isLogged()) {
      return (
        <form
          className="sign"
          onSubmit={this.submitForm(email, password, name, age)}
        >
          <div className="form-group">
            <input
              type="email"
              className="form-control"
              id="email"
              aria-describedby="emailHelp"
              placeholder="Email"
              onChange={this.changeEmail}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              className="form-control"
              id="password"
              placeholder="Password"
              onChange={this.changePassword}
            />
            <small id="emailHelp" className="form-text text-muted">
              {this.props.password_error}
            </small>
          </div>
          <div className="form-group">
            <input
              type="name"
              className="form-control"
              id="name"
              placeholder="Name"
              onChange={this.changeName}
            />
          </div>
          <div className="form-group">
            <input
              type="age"
              id="age"
              min="18"
              className="form-control"
              placeholder="Age"
              data-bind="value:age"
              onChange={this.changeAge}
            />
            <small id="emailHelp" className="form-text text-muted">
              {this.props.age_error}
            </small>
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      );
    }
    return <Redirect to="/" />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
