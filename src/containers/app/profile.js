import React from 'react';
import { connect } from 'react-redux';
import {
  UPDATE,
  GET_PROFILE,
  SET_PARAMS,
  AGE_ERROR,
  USER_CLEAR,
  UPDATE_USER_INFO,
} from '../../reducers/actions';
import request from '../../request';

const mapStateToProps = state => ({
  profile: state.profile,
  general: state.general,
});

const mapDispatchToProps = dispatch => ({
  onChange: (value, keyy) => dispatch({ type: SET_PARAMS, key: keyy, value}),
  onAgeError: () => dispatch({ type: AGE_ERROR }),
  onUnload: () => dispatch({ type: USER_CLEAR }),
  getProfileData: id => dispatch({ type: GET_PROFILE, payload: request.Users.byId(id) }),
  onSubmit: (email, name, age, id) =>
    dispatch({
      type: UPDATE,
      payload: request.Users.update(email, name, age, id),
    }),
  updateUserInfo: (_email, _name, _age, _description) => dispatch({
     type: UPDATE_USER_INFO,
     email: _email,
     name: _name,
     age: _age,
     description: _description,
   }),
});

class Profile extends React.Component {
    constructor() {
      super();
      this.changeEmail = ev => this.props.onChange(ev.target.value, 'email');
      this.changeName = ev => this.props.onChange(ev.target.value, 'name');
      this.changeAge = ev => this.props.onChange(ev.target.value, 'age')
      this.submitForm = (email, name, age) => ev => {
        let errNbr = false;
        ev.preventDefault();

        if (email === undefined)
          email = this.props.general.userInfo.email;
        if (name === undefined)
          name = this.props.general.userInfo.name;
        if (age === undefined)
          age = this.props.general.userInfo.age;

        if (age <= 18) {
          this.props.onAgeError();
          errNbr = true;
        }
        if (errNbr) return;
        this.props.onSubmit(email, name, age, this.props.general.userInfo.id);
      };
    }

    componentWillReceiveProps(props) {
      if (!props.profile.error && props.profile.payload !== undefined) {
        this.props.updateUserInfo(
          props.profile.payload.email,
          props.profile.payload.name,
          props.profile.payload.age,
          '',
        );
      }
    }

    componentWillMount() {
      let path = this.props.location.pathname;
      if (path !== '/profile') {
        this.props.getProfileData(path.substring(path.indexOf('/profile') + 9));
      }
    }

    componentWillUnmount() {
      this.props.onUnload();
    }

    render() {
      const user = (this.props.profile.profile !== undefined)
        ? this.props.profile.profile
        : this.props.general.userInfo;
      const { email, name, age, id } = this.props.profile;
      console.log("wut?", this.props.profile.profile);
      if (user) {
        return (
          <div className="row">
            <form
              className="user-settings col-6"
              onSubmit={this.submitForm(email, name, age)}
              >
              <div className="form-group profile">
                <label htmlFor="name">Username</label>
                <input
                  type="name"
                  className="form-control"
                  id="name"
                  placeholder={user.name}
                  onChange={this.changeName}
                  disabled={this.props.profile.profile !== undefined}
                />
                <label htmlFor="email">Email address</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  aria-describedby="emailHelp"
                  placeholder={user.email}
                  onChange={this.changeEmail}
                  disabled={this.props.profile.profile !== undefined}
                />
                <label htmlFor="age">Your age</label>
                <input
                  type="age"
                  id="age"
                  min="18"
                  className="form-control"
                  placeholder={user.age}
                  data-bind="value:age"
                  onChange={this.changeAge}
                  disabled={this.props.profile.profile !== undefined}
                />
                <small id="emailHelp" className="form-text text-muted">
                  {this.props.general.age_error}
                </small>

                <hr />
              </div>
              {this.props.profile.profile === undefined &&
                <button type="submit" className="btn tenten">
                  Save
                </button>}
            </form>
          </div>
        );
      } else {
        return (<div></div>);
      }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
