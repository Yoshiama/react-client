import React from 'react';
import { connect } from 'react-redux';
import Popup from 'reactjs-popup';
import {
  SEND_TAG,
  SET_PARAMS,
  ROOM_CLEAR,
  GET_CONVERSATIONS,
  GET_ROOMS,
  CREATE_ROOM,
  CLEAN_CONVERSATION_CALL,
  CHANGE_CONVERSATION,
} from '../../reducers/actions';
import request from '../../request';
import RoomList from './room-list';
import ConversationList from './conversation-list';
import Conversation from './conversation';

const mapStateToProps = state => ({
  room: state.room,
  general: state.general,
});

const mapDispatchToProps = dispatch => ({
  onChange: (value, key) => dispatch({ type: SET_PARAMS, key, value }),
  onChangePassword: value =>
    dispatch({ type: SET_PARAMS, key: 'password', value }),
  onSubmit: tag =>
    dispatch({ type: SEND_TAG, payload: request.Rooms.byTag(tag) }),
  onUnload: () => dispatch({ type: ROOM_CLEAR }),
  getRooms: () => dispatch({ type: GET_ROOMS, payload: request.Rooms.all() }),
  getConversations: () =>
    dispatch({
      type: GET_CONVERSATIONS,
      payload: request.Rooms.conversations(),
    }),
  createRoom: (name, tags, maxUsers, description) =>
    dispatch({
      type: CREATE_ROOM,
      payload: request.Rooms.create(name, tags, maxUsers, description),
    }),

  cleanConversationCall: () =>
    dispatch({
      type: CLEAN_CONVERSATION_CALL,
    }),

  changeConversation: conv =>
    dispatch({
      type: CHANGE_CONVERSATION,
      conv,
    }),
});

// import Request from 'request';

class Room extends React.Component {
  constructor() {
    super();
    this.changeTag = (ev) => {
      this.props.onChange(ev.target.value, 'tag');
      this.props.onSubmit(ev.target.value);
    };
    this.changeName = ev => this.props.onChange(ev.target.value, 'name');
    this.changeTags = ev => this.props.onChange(ev.target.value, 'tags');
    this.changeMaxUsers = ev =>
      this.props.onChange(ev.target.value, 'maxUsers');
    this.changeDescription = ev =>
      this.props.onChange(ev.target.value, 'description');

    this.onSubmitCreateRoom = (name, tags, maxUsers, description) => (ev) => {
      ev.preventDefault();
      const arrayTags = tags.split(' ');
      this.props.createRoom(name, arrayTags, maxUsers, description);
    };
  }

  componentWillMount() {
    this.props.getRooms();
    this.props.getConversations();
  }

  componentWillReceiveProps(props) {
    if (props.room.conversationCalled) {
      const id = props.room.roomId || '';
      const index = props.room.conversations.findIndex(
        room => room.id.toString() === id,
      );
      if (index !== -1) {
        this.props.changeConversation(props.room.conversations[index]);
      } else if (props.room.conversations.length > 1) {
        this.props.changeConversation(props.room.conversations[0]);
      }
    }
    if (props.room && props.room.isJoining === true) {
      if (props.room.joined === false && props.room.payload === 'Conflict') {
        const index = props.room.conversations.findIndex(
          room => room.id.toString() === props.room.roomId,
        );
        if (index !== -1) {
          props.changeConversation(props.room.conversations[index]);
        }
      } else if (props.room.joined === true) {
        props.getConversations();
      }
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { name, tags, maxUsers, description } = this.props.room;
    return (
      <div className="row">
        <aside className="rooms col-4">
          <form className="search-bar">
            <div className="form-group">
              <input
                type="tag"
                className="form-control"
                id="tag"
                aria-describedby="emailHelp"
                placeholder="tag"
                onChange={this.changeTag}
              />
            </div>
          </form>
          <Popup
            trigger={<button className="create-room btn">Create Room</button>}
            position="bottom left"
          >
            <div>
              <form
                className="name"
                onSubmit={this.onSubmitCreateRoom(
                  name,
                  tags,
                  maxUsers,
                  description,
                )}
              >
                <div className="form-group">
                  <input
                    type="name"
                    className="form-control"
                    id="name"
                    placeholder="Name"
                    onChange={this.changeName}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="tags"
                    className="form-control"
                    id="tags"
                    placeholder="Tags"
                    onChange={this.changeTags}
                  />
                </div>
                <div className="form-group">
                  <select name="maxUsers" onChange={this.changeMaxUsers}>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select>
                </div>
                <div className="form-group">
                  <input
                    type="description"
                    className="form-control"
                    id="description"
                    placeholder="Description"
                    onChange={this.changeDescription}
                  />
                </div>
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
          </Popup>
          <h2 className="title">All rooms </h2>
          <RoomList rooms={this.props.room.rooms} />
          <h2 className="title">Conversation</h2>
          <ConversationList conversations={this.props.room.conversations} />
        </aside>
        <section className="chat col-8">
          <Conversation room={this.props.room.actualConv} />
        </section>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Room);
