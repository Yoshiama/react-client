import React from 'react';
import { connect } from 'react-redux';
import {
  SEND_MESSAGE,
  LEAVE_ROOM,
  CONVERSATION_CLEAR,
  SET_PARAMS,
  NOTIFICATION,
  SERVER_RESPONSE,
  CONVERSTION_INIT,
} from '../../reducers/actions';
import request from '../../request';

const mapStateToProps = state => ({
  general: state.general,
  conversation: state.conversation,
});

const mapDispatchToProps = dispatch => ({
  sendMessage: () => dispatch({ type: SEND_MESSAGE }),
  leaveRoom: id => dispatch({ type: LEAVE_ROOM, id }),
  onUnload: () => dispatch({ type: CONVERSATION_CLEAR }),
  kickMember: id => dispatch({ type: LEAVE_ROOM, id }),
  onChange: (value, key) => dispatch({ type: SET_PARAMS, key, value }),
  onNotification: message => dispatch({ type: NOTIFICATION, message }),
  onServerResponse: response => dispatch({ type: SERVER_RESPONSE, response }),
  onConversationInit: () => dispatch({ type: CONVERSTION_INIT }),
});

// import Request from 'request';

class Conversation extends React.Component {
  constructor() {
    super();
    this.changeText = ev => this.props.onChange(ev.target.value, 'text');

    this.onSendMessage = (ev) => {
      this.inputEntry.value = '';
      ev.preventDefault();
      const message = {
        auth: window.localStorage.getItem('jwt'),
        room: this.props.room.id,
        message: this.props.conversation.text,
      };
      request.Rooms.sendMessage(this.props.room.id, message);
      return false;
    };

    this.onEnterPressed = (ev) => {
      if (ev.key === 'Enter') {
        this.onSendMessage(ev);
      }
    };
  }

  componentWillMount() {}

  componentWillReceiveProps(props) {
    if (!props.conversation.init) {
      const data = {
        auth: window.localStorage.getItem('jwt'),
      };
      request.Rooms.connect(data);
      request.Rooms.chatroom(props.room.id, props.onNotification);
      this.props.onConversationInit();
      console.log('dd');
    }
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    if (this.props.room) {
      return (
        <div>
          <div className="conversation-header">
            <div className="info">
              <h2>Title</h2>
              <ol>user list</ol>
            </div>
            <div className="action-btn">
              <button className="btn">KICK IT</button>
              <button className="btn">Cé bon jme cass ca ma saoulé !</button>
            </div>
          </div>
          <div className="conversation-container">
            <ul className="clavardage">
              {this.props.conversation.notification.map(
                m => m.type && m.type === 'MESSAGE' && <li>{m.data.message}</li>,
              )}
            </ul>
          </div>
          <div>
            <div className="input-zone" action="/action_page.php">
              <input
                className="chat-input"
                type="text"
                name="fname"
                onChange={this.changeText}
                onKeyPress={this.onEnterPressed}
                ref={(el) => {
                  this.inputEntry = el;
                }}
              />
              <button
                className="chat-sent"
                type="button"
                value="Send"
                onClick={this.onSendMessage}
              >
                Send
              </button>
            </div>
          </div>
        </div>
      );
    }
    return <div></div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Conversation);
