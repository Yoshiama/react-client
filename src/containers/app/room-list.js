import React from 'react';
import { connect } from 'react-redux';
import { JOIN_ROOM, ROOM_LIST_CLEAR } from '../../reducers/actions';
import request from '../../request';

const mapStateToProps = state => ({ room: state.room });

const mapDispatchToProps = dispatch => ({
  joinRoom: id =>
    dispatch({ type: JOIN_ROOM, payload: request.Rooms.join(id), id }),
  // leaveRoom: id =>
  // dispatch({ type: LEAVE_ROOM, payload: request.Rooms.leave(id) }),
  onUnload: () => dispatch({ type: ROOM_LIST_CLEAR }),
});

class RoomList extends React.Component {
  constructor() {
    super();
    this.onJoin = (ev) => {
      this.props.joinRoom(ev.target.id);
    };
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    if (Array.isArray(this.props.rooms)) {
      return (
        <ul className="rooms-list">
          {this.props.rooms.map(r => (
            <li key={r.id} className="items-room">
              <h3> {r.name} </h3>
              <p className="description">{r.description} </p>
              <div>
                <span>
                  User: {r.members.length}/{r.maxUsers}
                </span>
                {r.tags.map(t => (
                  <span className="tags" key={t}>
                    {t}
                  </span>
                ))}
              </div>
              <button
                className="btn join"
                type="button"
                onClick={this.onJoin}
                id={r.id}
              >
                Join
              </button>
            </li>
          ))}
        </ul>
      );
    }
    return <ul />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomList);
