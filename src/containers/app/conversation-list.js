import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = state => ({ ...state.room });

class ConversationList extends React.Component {
  render() {
    if (Array.isArray(this.props.rooms)) {
      return (
        <ul className="rooms-list">
          {this.props.rooms.map(r => (
            <li key={r.id} className="items-room">
              <h3> {r.name} </h3>
              <p className="description">{r.description} </p>
              <div>
                <span>
                  User: {r.members.length}/{r.maxUsers}
                </span>
                {r.tags.map(t => <span className="tags">{t}</span>)}
              </div>
            </li>
          ))}
        </ul>
      );
    }
    return <ul />;
  }
}

export default connect(mapStateToProps, null)(ConversationList);
