import React from 'react';
import { connect } from 'react-redux';
import { Route, NavLink, Switch } from 'react-router-dom';
import { push } from 'react-router-redux';
import Jwt from 'jsonwebtoken';
import store from '../../store';
import Login from './login';
import Profile from './profile';
import Register from './register';
import request from '../../request';
import Room from './room';
import {
  CLEAN_GOTO,
  APP_INIT,
  SET_USER_INFO,
  LOGOUT,
} from '../../reducers/actions';

const mapStateToProps = state => ({
  login: state.login,
  general: state.general,
});

const mapDispatchToProps = dispatch => ({
  onInit: (id, info) => dispatch({ type: APP_INIT, info }),
  cleanGoto: () => dispatch({ type: CLEAN_GOTO }),
  onClickLogout: () => dispatch({ type: LOGOUT }),
  setUserInfo: info => dispatch({ type: SET_USER_INFO, user: info }),
});

class App extends React.Component {
  componentWillMount() {
    if (!this.props.general.userInfo) {
      const token = window.localStorage.getItem('jwt');
      let result = null;

      if (token) {
        request.setToken(token);
        result = Jwt.decode(token);
      }

      if (result) {
        this.props.onInit(result.id, result);
      } else store.dispatch(push('/login'));
    }
  }

  componentWillReceiveProps(props) {
    if (props.login.goTo) {
      if (props.login.loggedIn) {
        this.props.setUserInfo(props.login.payload);
      }
      store.dispatch(push(props.login.goTo));
      this.props.cleanGoto();
    }
  }

  render() {
    return (
      <div className="container">
        <header className="row header">
          <div>
            {this.props.general.userInfo && (
              <NavLink exact to="/">
                Home
              </NavLink>
            )}
            {this.props.general.userInfo && (
              <NavLink exact to="/profile">
                {this.props.general.userInfo.name}
              </NavLink>
            )}
            {!this.props.general.userInfo && (
              <NavLink to="/login">Login</NavLink>
            )}
            {!this.props.general.userInfo && (
              <NavLink to="/register">Register</NavLink>
            )}
          </div>
          <div>
            {this.props.general.userInfo && (
              <NavLink
                className="logout-btn"
                to="/login"
                onClick={this.props.onClickLogout}
              >
                Logout
              </NavLink>
            )}
          </div>
        </header>

        <div>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route path="/profile" component={Profile} />
            <Route path="/" component={Room} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
