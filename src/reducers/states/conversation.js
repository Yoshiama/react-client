import {
  CONVERSATION_CLEAR,
  SEND_MESSAGE,
  SET_PARAMS,
  KICK_MEMBER,
  LEAVE_ROOM,
  NOTIFICATION,
  SERVER_RESPONSE,
  CONVERSTION_INIT,
} from '../actions';

export default (state = { notification: [], init: false }, action) => {
  switch (action.type) {
    case SET_PARAMS:
      return { ...state, [action.key]: action.value };
    case LEAVE_ROOM:
    case KICK_MEMBER:
    case SEND_MESSAGE:
      return {
        ...state,
      };
    case NOTIFICATION:
      return {
        ...state,
        notification: [...state.notification, action.message],
      };
    case SERVER_RESPONSE:
      return {
        ...state,
        response: action.response,
      };
    case CONVERSTION_INIT:
      state.init = true;
      return {
        ...state,
      };
    case CONVERSATION_CLEAR:
      state.notification = [];
      state.init = false;
      return {};
    default:
      return state;
  }
};
