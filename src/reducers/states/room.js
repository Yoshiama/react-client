import {
  SET_PARAMS,
  ROOM_CLEAR,
  SEND_TAG,
  GET_ROOMS,
  GET_CONVERSATIONS,
  CREATE_ROOM,
  JOIN_ROOM,
  ROOM_LIST_CLEAR,
  CHANGE_CONVERSATION,
  CLEAN_CONVERSATION_CALL,
} from '../actions';

export default (state = { messages: [], loadConv: 0 }, action) => {
  switch (action.type) {
    case SEND_TAG:
      return {
        ...state,
        roomError: action.error,
        rooms: action.error ? state.rooms : action.payload,
      };
    case SET_PARAMS:
      return { ...state, [action.key]: action.value };
    case GET_ROOMS:
      return {
        ...state,
        roomError: action.error,
        rooms: action.error ? [] : action.payload,
      };
    case GET_CONVERSATIONS:
      return {
        ...state,
        conversationError: action.error,
        conversations: action.error ? [] : action.payload,
        conversationCalled: true,
      };
    case CREATE_ROOM:
      return {
        ...state,
        rooms: action.error ? state.rooms : [...state.rooms, action.payload],
        conversations: action.error
          ? state.rconversationsooms
          : [...state.conversations, action.payload],
      };
    case CHANGE_CONVERSATION:
      return {
        ...state,
        joined: null,
        roomId: null,
        actualConv: action.conv,
        isJoining: null,
        conversationCalled: null,
      };
    case CLEAN_CONVERSATION_CALL:
      return {
        ...state,
        conversationCalled: null,
        roomId: action.id,
      };
    case JOIN_ROOM:
      return {
        ...state,
        joined: !action.error,
        payload: action.error ? action.payload.error : action.payload,
        isJoining: true,
        roomId: action.id,
      };
    case ROOM_LIST_CLEAR:
    case ROOM_CLEAR:
      return {};
    default:
      return state;
  }
};
