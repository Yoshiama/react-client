import {
  GET_PROFILE,
  UPDATE,
  SET_PARAMS,
  AGE_ERROR,
  USER_CLEAR,
} from '../actions';

export default (state = {}, action) => {
  switch (action.type) {
    case GET_PROFILE:
      return {
        ...state,
        profileError: action.error,
        profile: action.payload,
      };
    case UPDATE:
      return {
        ...state,
        error: action.error,
        payload: action.payload,
      };
    case SET_PARAMS:
      return { ...state, [action.key]: action.value };
    case AGE_ERROR:
      return {
        ...state,
        age_error: 'frr té tro jeun, fo avoir 18an ici, azy casse toi',
      };
    case USER_CLEAR:
      return {};
    default:
      return state;
  }
};
