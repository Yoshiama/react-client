import {
  LOGIN,
  REGISTER,
  SET_PARAMS,
  PASSWORD_ERROR,
  AGE_ERROR,
  LOGIN_CLEAR,
  REGISTER_CLEAR,
} from '../actions';

export default (state = {}, action) => {
  switch (action.type) {
    case LOGIN:
    case REGISTER:
      return {
        ...state,
        loggedIn: true,
        goTo: action.error === true ? null : '/',
        error: action.error,
        payload: action.payload,
      };
    case SET_PARAMS:
      return { ...state, [action.key]: action.value };
    case PASSWORD_ERROR:
      return {
        ...state,
        password_error: 'té niké frr. mé + de caractr, genr 8, 8 cé b1',
      };
    case AGE_ERROR:
      return {
        ...state,
        age_error: 'frr té tro jeun, fo avoir 18an ici, azy casse toi',
      };
    case LOGIN_CLEAR:
    case REGISTER_CLEAR:
      return {};
    default:
      return state;
  }
};
