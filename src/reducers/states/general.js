import {
  CLEAN_GOTO,
  APP_INIT,
  SET_USER_INFO,
  LOGOUT,
  UPDATE_USER_INFO,
} from '../actions';

export default (
  state = {
    userInfo: null,
    init: false,
    notification: [],
  },
  action,
) => {
  switch (action.type) {
    case CLEAN_GOTO:
      return {
        ...state,
        goTo: null,
      };
    case SET_USER_INFO:
      state.userInfo = action.user;
      return state;
    case APP_INIT:
      state.init = true;
      state.userInfo = action.info;
      return state;
    case UPDATE_USER_INFO:
      state.userInfo.email = action.email;
      state.userInfo.name = action.name;
      state.userInfo.age = action.age;
      state.userInfo.description = action.description;
      return state;
    case LOGOUT:
      state.userInfo = null;
      return {
        ...state,
        goTo: '/',
      };
    default:
      return state;
  }
};
