import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import login from './states/login';
import general from './states/general';
import room from './states/room';
import conversation from './states/conversation';
import profile from './states/profile';

export default combineReducers({
  routing: routerReducer,
  login,
  general,
  room,
  conversation,
  profile,
});
