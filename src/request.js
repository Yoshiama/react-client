import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import io from 'socket.io-client';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://localhost:4000';

const responseBody = res => res.body;
const socket = io.connect(API_ROOT);
let token = null;

const tokenPlugin = (req) => {
  if (token) {
    req.set('Authorization', `Bearer ${token}`);
  }
};

const requests = {
  del: url =>
    superagent
      .del(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .then(responseBody),
  get: url =>
    superagent
      .get(`${API_ROOT}${url}`)
      .accept('application/json')
      .use(tokenPlugin)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .accept('application/json')
      .use(tokenPlugin)
      .then(responseBody),
  patch: (url, body) =>
    superagent
      .patch(`${API_ROOT}${url}`, body)
      .accept('application/json')
      .use(tokenPlugin)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .accept('application/json')
      .use(tokenPlugin)
      .then(responseBody),
  postImage: (url, imagePath) =>
    superagent
      .post(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .attach('file', imagePath)
      .then(responseBody),
};

const Auth = {
  login: (email, password) =>
    requests.post('/login', {
      email,
      password,
    }),
  register: (email, password, name, age, description) =>
    requests.post('/users', {
      email,
      password,
      name,
      age,
      description,
    }),
};

const Users = {
  all: () => requests.get('/users'),
  byId: id => requests.get(`/users/${id}`),
  update: (email, name, age, id) =>
    requests.patch(`/users/${id}`, {
      email,
      name,
      age,
    }),
  getPicture: id => requests.get(`/pictures/${id}`),
  updatePicture: imagePath =>
    requests.postImage(`/users/picture`, { imagePath }),
};

const Rooms = {
  messages: id => requests.post(`/rooms/${id}/messages`),
  byTag: tag => requests.get(`/rooms/tag/${tag}`),
  all: () => requests.get(`/rooms`),
  join: id => requests.post(`/rooms/${id}/join`, null),
  members: id => requests.get(`/rooms/${id}/members`),
  create: (name, tags, maxUsers, description) =>
    requests.post(`/rooms`, {
      name,
      tags,
      maxUsers,
      description,
    }),
  conversations: () => requests.get('/rooms/conversations'),
  delete: id => requests.delete(`/rooms/${id}`),
  leave: id => requests.post(`/rooms/${id}/leave`),
  kickUser: (id, anonymeId) =>
    requests.post(`/rooms/${id}/kick`, {
      anonymeId,
    }),
  reveal: id => requests.post(`/rooms/${id}/reveal`),
  connect: (message, queue = []) => {
    socket.emit('connected', message);
    socket.on('connected', (data) => {
      queue.push(data);
    });
  },
  chatroom: (id, callback) => {
    socket.on(id, (message) => {
      console.log(message);
      callback(message);
    });
  },
  sendMessage: (id, message) => {
    console.log('sock: ', id, message);
    socket.emit(id, message);
  },
};

export default {
  Auth,
  Users,
  Rooms,
  setToken: (_token) => {
    token = _token;
  },
  token,
};
